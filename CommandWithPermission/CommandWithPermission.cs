﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RMP.Data;
using RMP.Hepler;

namespace RMP.Commands
{
   /// <summary>
   /// Команда с проверкой разрешения
   /// </summary>
   public abstract class CommandWithPermission : ICommand
   {
      /// <summary>
      /// AdminDataProvider
      /// </summary>
      private IAdminDataProvider _adminDataProvider = new AdminDataProvider();

      public virtual bool CanExecute(object parameter)
      {
         if (Utils.IsInDesignerMode)
            return true;
#if DEBUG
         return true;
#else
         return _adminDataProvider.CanExecuteCommand(this.GetType().Name);
#endif
      }

      public event EventHandler CanExecuteChanged;

      public abstract void Execute(object parameter);

      public abstract async Task ExecuteAsync(object parameter);
   }
}
