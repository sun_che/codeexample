﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RMP.Tasks;
using RMP.Hepler;

namespace RMP.Commands
{
   /// <summary>
   /// Команды доступные администратору
   /// </summary>
   [Description("Удаление проектов.")]
   public class RemoveProjectCommand : CommandWithPermission
   {
      private Action<object> _executeMethod;

      public override void Execute(object parameter)
      {
         if (Utils.IsInDesignerMode)
            return;

         _executeMethod(parameter);
      }

      public override async Task ExecuteAsync(object parameter)
      {
         if (Utils.IsInDesignerMode)
            return;

         await Task.Run(() => _executeMethod(parameter));
      }

      /// <summary>
      /// Удаление проекта
      /// </summary>
      public static CommandWithPermission RemoveProject
      {
         get
         {
            return new RemoveProjectCommand { _executeMethod = ProjectTasks.DeleteProject };
         }
      }
   }
}
