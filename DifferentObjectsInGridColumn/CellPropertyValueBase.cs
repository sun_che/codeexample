﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using RMP.Data.Helper;
using RMP.Data.Interface.Property;

namespace RMP.Data.Project.PropertyValue
{
   public abstract class CellPropertyValueBase : ICellPropertyValue
   {
      public event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Наименование свойства объекта
      /// </summary>
      public string PropertyName { get; set; }
      public abstract object Value { get; set; }
      public abstract object ValueObject { get; }

      protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
      {
         PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
      }

      // Позвязываем на изменение реального объекта, чтобы потом оповестить грид
      protected void ValuePropertyChanged(object sender, PropertyChangedEventArgs e)
      {
         if(e.PropertyName.Equals(PropertyName))
         {
            OnPropertyChanged(nameof(Value));
         }
      }

      public abstract void Dispose();
   }
}
