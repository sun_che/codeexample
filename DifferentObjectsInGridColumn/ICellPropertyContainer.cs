﻿using System;
using System.Collections.Generic;

namespace RMP.Data.Interface.Property
{
   /// <summary>
   /// Используется как строка для грида
   /// </summary>
   public interface ICellPropertyContainer : IDisposable
   {
      /// <summary>
      /// Значение для группировки
      /// </summary>
      object GroupValue { get; set; }

      /// <summary>
      /// Коллекция значений
      /// </summary>
      IList<ICellPropertyValue> Values { get; set; }

      /// <summary>
      /// Наименование шаблона для контрола ячейки
      /// </summary>
      string CellEditTemplateName { get; set; }
   }
}
