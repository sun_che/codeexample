﻿using System;
using System.ComponentModel;

namespace RMP.Data.Interface.Property
{
   /// <summary>
   /// Используется, как значение для ячейки грида
   /// Для передачи в колонку грида разных свойств
   /// </summary>
   public interface ICellPropertyValue : IDisposable, INotifyPropertyChanged
   {
      /// <summary>
      /// Наименование свойства объекта
      /// </summary>
      string PropertyName { get; set; }
       
      /// <summary>
      /// Значение которое будет возвращать объект
      /// </summary>
      object Value { get; set; }

      /// <summary>
      /// Объект свойства
      /// </summary>
      object ValueObject { get; }
   }
}