﻿using System.Collections.Generic;
using System.Linq;
using RMP.Data.Helper;
using RMP.Data.Interface.ProjectData;

namespace RMP.Data.Project.PropertyValue
{
   /// <summary>
   /// Trp для недель
   /// Когда в неделе меняется месяц 
   /// </summary>
   public class ProjectMergedWeekTrpValue : CellPropertyValueBase
   {
      private IEnumerable<IProjectMonitoringPeriod> _weeks;

      public ProjectMergedWeekTrpValue(params IProjectMonitoringPeriod[] weeks)
      {
         _weeks = weeks;
         PropertyName = nameof(_weeks.First().Trp);
         foreach (var week in _weeks)
         {
            week.PropertyChanged += ValuePropertyChanged;
         }
      }

      public override object Value
      {
         get { return _weeks.Sum(x => x.Trp); }
         set
         {
            var trp = value as double;
            if(trp == null)
               return;
            // распределяем пропорционально количеству дней
            var daysSum = _weeks.Sum(x => x.NumOfActiveDays);
            if(daysSum == 0)
               return;
            foreach (var week in _weeks)
            {
               var weekPortion = (double)week.NumOfActiveDays / daysSum;
               week.Trp = trp * weekPortion;
            }
         }
      }

      public override object ValueObject
      {
         get { return _weeks; }
      }

      public override void Dispose()
      {
         foreach (var week in _weeks)
         {
            week.PropertyChanged -= ValuePropertyChanged;
         }
      }
   }
}