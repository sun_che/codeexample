﻿using RMP.Data.Helper;
using RMP.Data.Interface.ProjectData;

namespace RMP.Data.Project.PropertyValue
{
   /// <summary>
   /// Trp недели
   /// </summary>
   public class ProjectWeekTrpValue : CellPropertyValueBase
   {
      private IProjectMonitoringPeriod _week;
      public ProjectWeekTrpValue(IProjectMonitoringPeriod week)
      {
         _week = week;
         _week.PropertyChanged += ValuePropertyChanged;
         PropertyName = nameof(_week.Trp);
      }

      public override object Value
      {
         get { return _week.Trp; }
         set
         {
            var trp = value as double;
            if(trp != null)
               _week.Trp = trp;
         }
      }

      public override object ValueObject
      {
         get { return _week; }
      }

      public override void Dispose()
      {
         _week.PropertyChanged -= ValuePropertyChanged;
      }
   }
}