﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RMP.Data;
using RMP.Data.Base;
using RMP.Data.Calendar;
using RMP.Data.Cost;
using RMP.Data.DataManipulate;
using RMP.Data.Interface;
using RMP.Data.Permission;
using RMP.Data.Project.ProjectDataAnalyse;
using RMP.Data.Project.ReferenceData;
using RMP.Extension;
using CurrProperties = RMP.Data.Properties.Settings;

namespace RMP.Data
{
   /// <summary>
   /// Методы расширения для членов контекста данных
   /// </summary>
   public static class ContextExtensions
   {
      /// <summary>
      /// Найти элемент в БД
      /// </summary>
      public static T GetItem<T>(this DbSet<T> table, string value) where T : ReferenceItem
      {
         return table.FirstOrDefault(x => x.Value.Equals(value, StringComparison.OrdinalIgnoreCase));
      }

      /// <summary>
      /// Содержит ли таблица элемент со значением value
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="table">The table.</param>
      /// <param name="value">The value.</param>
      /// <returns></returns>
      public static bool ContainItem<T>(this DbSet<T> table, string value) where T : ReferenceItem
      {
         return table.Any(x => x.Value.Equals(value, StringComparison.OrdinalIgnoreCase));
      }

      /// <summary>
      /// Добавляет элемент в справочник
      /// </summary>
      private static T AddItem<T>(this DbSet<T> table, string value) where T : ReferenceItem
      {
         var newItem = (T) Activator.CreateInstance(typeof(T), new object[] { value });
         table.Add(newItem);

         return newItem;
      }
   }
}
