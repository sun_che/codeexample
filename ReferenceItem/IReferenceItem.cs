﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMP.Data.Interface
{
   /// <summary>
   /// Используется как объект таблицы справочной информации
   /// Для одиноковых справочных таблиц ключ - значение
   /// </summary>
   public interface IReferenceItem
   {
      /// <summary>
      /// Id
      /// </summary>
      [Key]
      int Id { get;set;}
      
      /// <summary>
      /// Справочное значение
      /// </summary>
      [Index(IsUnique = true)]
      [Required]
      string Value { get; set; }
   }
}
