﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RMP.Data.Interface;

namespace RMP.Data.Base
{
   /// <summary>
   /// /// <summary>
   /// Используется как объект таблицы справочной информации
   /// </summary>
   /// </summary>
   public class ReferenceItem : IReferenceItem, IComparable
   {
      /// <summary>
      /// Id
      /// </summary>
      public virtual int Id { get; set; }

      /// <summary>
      /// Справочное значение
      /// </summary>
      public virtual string Value { get; set; }

      public ReferenceItem() { }

      public ReferenceItem(string value)
      {
         Value = value;
      }

      public override string ToString()
      {
         return Value.ToString();
      }

      public int CompareTo(object obj)
      {
         return Value.CompareTo(obj.ToString());
      }
   }
}