﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RMP.Data.Base;
using RMP.Data.Interface;

namespace RMP.Data.Reference
{        
   /// <summary>
   /// Регион
   /// </summary>
   public class Region : ReferenceItem
   {
      [MaxLength(128)]
      [Index(IsUnique = true)]
      [Required]
      public override string Value { get; set; }

      public Region() {}

      public Region(string value) : base(value) {}
   }
}
