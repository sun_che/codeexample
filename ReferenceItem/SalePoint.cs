﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RMP.Data.Base;
using RMP.Data.Interface;

namespace RMP.Data
{
   /// <summary>
   /// Точка продаж
   /// </summary>
   public class SalePoint : ReferenceItem
   {
      [MaxLength(64)]
      [Index(IsUnique = true)]
      [Required]
      public override string Value { get; set; }

      /// <summary>
      /// Наименование считалки
      /// </summary>
      [MaxLength(256)]
      public string CalcName { get; set; }

      /// <summary>
      /// Продается ли канал через считалку, или сам
      /// </summary>
      [Index(IsUnique = false)]
      [DefaultValue(false)]
      public bool IsCalc { get; set; }

      public SalePoint() : base() { }

      public SalePoint(string value) : base(value) { }

      
   }
}