﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using Goods4Cast.EC.ViewModel.Resources;
using Goods4Cast.GUI.Common;

namespace Goods4Cast.EC.ViewModel.Workspaces.Dimension
{
    /// <summary>
    /// Обобщенный класс для отображения элемента роли с возможностью перещелкивания прав для Autocompletebox
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="Caliburn.Micro.PropertyChangedBase" />
    public class RoleViewModel<T> : PropertyChangedBase where T : struct, IConvertible, IComparable
    {
        /// <summary>
        /// Коллекция объектов прав
        /// </summary>
        private readonly List<DimensionRightsValue> _rightsCollection;

        /// <summary>
        /// Отображаемое значение права
        /// </summary>
        private DimensionRightsValue _dimensionValue;

        private string _icon;

        private string _name;

        private T _rights;

        public RoleViewModel(int id, string name, T rights, List<DimensionRightsValue> rightsCollection)
        {
            Id = id;
            Name = name;
            _rightsCollection = rightsCollection;
            _rights = rights;
            _dimensionValue = _rightsCollection.FirstOrDefault(x => x.Value == Convert.ToByte(rights));
            UpdateImage();
        }

        /// <summary>
        /// Отрабатывает клик по кнопке
        /// Изменяет выбранное значение разрешения роли
        /// </summary>
        /// <value>
        /// The change dimension command.
        /// </value>
        public ICommand ChangeDimensionCommand => new DelegateCommand(ChangeDimansion);


        /// <summary>
        /// Наименование роли
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Строка к картинке
        /// </summary>
        /// <value>
        /// The icon.
        /// </value>
        public string Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Подсказка, которая всплыает при наведении на кнопку смены прав
        /// </summary>
        /// <value>
        /// The tool tip.
        /// </value>
        public string ToolTip => $"{DimensionToolTip.ToolTip}{DimensionValue.Description}";

        /// <summary>
        /// Разрешения роли
        /// </summary>
        /// <value>
        /// The rights.
        /// </value>
        public T Rights
        {
            get { return _rights; }
            set
            {
                if (value.Equals(_rights)) return;
                _rights = value;

                OnRightChanged();
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Элемент для отображения кнопки
        /// </summary>
        /// <value>
        /// The dimension value.
        /// </value>
        // Использовал тот же код, что и был для настройик прав, чтобы не городить нового
        public DimensionRightsValue DimensionValue
        {
            get { return _dimensionValue; }
            set
            {
                if (value == _dimensionValue) return;
                _dimensionValue = value;
                NotifyOfPropertyChange();
            }
        }

        /// <summary>
        /// Коллекция разрешений роли
        /// </summary>
        /// <value>
        /// The rights collection.
        /// </value>
        public List<DimensionRightsValue> RightsCollection => _rightsCollection;

        /// <summary>
        /// Идентификатор роли
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Меняет выбранный элемент прав роли, если права роли поменялись извне
        /// </summary>
        private void OnRightChanged()
        {
            var dimensionValue = RightsCollection.FirstOrDefault(x => x.Value == Convert.ToByte(Rights));
            if (dimensionValue == null) return;

            DimensionValue = dimensionValue;
            UpdateImage();
        }

        private void UpdateImage()
        {
            Icon = ImageHelper.GetRightImage(Rights.ToString());
        }

        /// <summary>
        /// Метод изменения разрещения роли
        /// </summary>
        /// <param name="obj">The object.</param>
        protected virtual void ChangeDimansion(object obj)
        {
            var nextIndex = _rightsCollection.IndexOf(_dimensionValue) + 1;
            if (nextIndex == _rightsCollection.Count)
                nextIndex = 0;
            DimensionValue = _rightsCollection[nextIndex];
            Rights = (T) (object) DimensionValue.Value;

            // чтобы не отрабатывалось поведение RadAutoBox
            var eventArgs = obj as RoutedEventArgs;
            if (eventArgs != null)
                eventArgs.Handled = true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RoleViewModel<T>) obj);
        }

        protected bool Equals(RoleViewModel<T> other)
        {
            return Id == other.Id && string.Equals(Name, other.Name);
        }

        public override int GetHashCode()
        {
            // Взял из нашего кода
            unchecked
            {
                var hashCode = Id;
                hashCode = (hashCode*397) ^ (Name?.GetHashCode() ?? 0);
                return hashCode;
            }
        }
    }
}