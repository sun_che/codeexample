﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;

namespace RMP.Converter
{
   public class GroupSummaryConverter : IMultiValueConverter
   {
      public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
      {
         GridColumn column = values[0] as GridColumn;
         IEnumerable<GridGroupSummaryData> data = values[1] as IEnumerable<GridGroupSummaryData>;
         
         if (column == null && data == null)
            return null;
         var items = (from gridGroupSummaryData in data where gridGroupSummaryData.Column == column select gridGroupSummaryData).ToList();

         if (items.Count == 0)
         {
            return null;
         }
         return items;
      }

      public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
      {
         return null;
      }
   }
}
